﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    public float moveSpeed;
    public float jumpForce;
    public LayerMask whatIsGround;
    public LayerMask whatIsWall;

    private bool rightDirection = true;
    private Rigidbody2D myRigidbody;
    private Collider2D myCollider;
    private bool grounded;
    private bool walled;
    private float delayCheckWall = 0;
	// Use this for initialization
	void Start () {
        myRigidbody = transform.GetComponent<Rigidbody2D>();
        myCollider = transform.GetComponent<Collider2D>();
	}
	
	// Update is called once per frame
	void Update () {
        grounded = Physics2D.IsTouchingLayers(myCollider, whatIsGround);
        walled = Physics2D.IsTouchingLayers(myCollider,whatIsWall);

        if (delayCheckWall > 0)
            delayCheckWall -= Time.deltaTime;
        if (walled && delayCheckWall <= 0)
        {
            delayCheckWall = 1;
            rightDirection = !rightDirection;
            transform.rotation = Quaternion.Euler(0, (rightDirection ? 0 : 180), 0);
        }

     //   Vector2 moveStep = new Vector2();

        myRigidbody.velocity = new Vector2(rightDirection?moveSpeed:-moveSpeed, myRigidbody.velocity.y);
        if (Input.GetMouseButtonDown(0))
        {
            if(grounded)
                myRigidbody.velocity = new Vector2(myRigidbody.velocity.x, jumpForce);
        }
	}
}
